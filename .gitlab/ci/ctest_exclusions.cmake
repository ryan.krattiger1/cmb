set(test_exclusions
  # Needs looked at.
  # Maybe https://gitlab.kitware.com/paraview/paraview/merge_requests/3728 fixes it?
  "ColorKnee"
)
string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()
