cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20210420
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "60ef19132fa25629b96c36c9")
  set(file_hash "4ea7c3f58b0e017667653617be7e1515740753f2250ef089ad370666f5c57e47dfc038d26caa8a070c24b695fac08482fd92a543d627e6bff1db7d03f9e01340")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_arm64")
  set(file_item "60ef18f82fa25629b96c36a4")
  set(file_hash "dd2a7f9d6a0801e858c058d08fbf4a55ea68c2813a24351777668e4a834236db7a3e75a0e240ed72cb92c0ffc2b73d73217aadf9c52be63a4c14940517c62a95")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos_x86_64")
  set(file_item "60ef18ce2fa25629b96c35b6")
  set(file_hash "a0a9deef383173abaa31dd75a3f03c6e3010d5313afd889b6eba7a9f040c1497e39ba2372fe2ac4a8aa3d06c3e444cb76b6d5fb4a5ff4c74c031690bc8e22b46")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
