//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "modelbuilder/mbTestEventTranslator.h"
#include "modelbuilder/mbTestData.h"

#include "pqCoreTestUtility.h"
#include "pqFileDialog.h"

#include <QDir>
#include <QEvent>
#include <QtDebug>

mbTestEventTranslator::mbTestEventTranslator(QObject* p)
  : pqWidgetEventTranslator(p)
{
}

bool mbTestEventTranslator::translateEvent(QObject* Object, QEvent* Event, bool& /*Error*/)
{
  // Capture input for pqFileDialog and all its children ...
  pqFileDialog* object = nullptr;
  for (QObject* o = Object; o; o = o->parent())
  {
    object = qobject_cast<pqFileDialog*>(o);
    if (object)
    {
      break;
    }
  }

  if (!object)
  {
    return false;
  }

  if (Event->type() == QEvent::FocusIn && !m_currentObject)
  {
    m_currentObject = object;
    connect(
      object, SIGNAL(fileAccepted(const QString&)), this, SLOT(onFilesSelected(const QString&)));
    connect(object, SIGNAL(rejected()), this, SLOT(onCancelled()));
  }

  return true;
}

void mbTestEventTranslator::onFilesSelected(const QString& file)
{
  QString cmb_data_directory(cmb_data_dir);
  cmb_data_directory = QDir::cleanPath(QDir::fromNativeSeparators(cmb_data_directory));
  if (cmb_data_directory.isEmpty())
  {
    qWarning() << "You must set the cmb_data_dir cmake variable to play-back file selections.";
  }

  QString test_data_directory(cmb_test_data_dir);
  test_data_directory = QDir::cleanPath(QDir::fromNativeSeparators(test_data_directory));
  if (test_data_directory.isEmpty())
  {
    qWarning() << "You must set the cmb_test_data_dir cmake variable to play-back file selections.";
  }

  QString cleanedFile = QDir::cleanPath(QDir::fromNativeSeparators(file));

  if (cleanedFile.indexOf(cmb_data_directory, 0, Qt::CaseInsensitive) == 0)
  {
    cleanedFile.replace(cmb_data_directory, "$cmb_data_dir", Qt::CaseInsensitive);
  }
  else if (cleanedFile.indexOf(test_data_directory, 0, Qt::CaseInsensitive) == 0)
  {
    cleanedFile.replace(test_data_directory, "$cmb_test_data_dir", Qt::CaseInsensitive);
  }
  else
  {
    qWarning() << "You must choose a file under the cmb_data_dir "
               << "or cmb_test_data_dir directories to record file selections.";
  }

  // XXX(clang-tidy): The linter sees `emit` go away from the preprocessor and
  // sees the line with 3 spaces for indentation. This makes it question
  // whether it should be part of the previous `if` block. Ignore this.
  // NOLINTNEXTLINE(readability-misleading-indentation)
  emit recordEvent(m_currentObject, "filesSelected", cleanedFile);
}

void mbTestEventTranslator::onCancelled()
{
  emit recordEvent(m_currentObject, "cancelled", "");
}
