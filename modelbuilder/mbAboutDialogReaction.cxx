//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "mbAboutDialogReaction.h"

#include "modelbuilder/mbVersion.h"

#include "pqAboutDialog.h"
#include "pqCoreUtilities.h"

#include <QApplication>
#include <QLabel>
#include <QTabWidget>
#include <QTextBrowser>
#include <QTreeWidget>

//-----------------------------------------------------------------------------
mbAboutDialogReaction::mbAboutDialogReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void mbAboutDialogReaction::showAboutDialog()
{
  pqAboutDialog aboutDialog(pqCoreUtilities::mainWidget());

  aboutDialog.setWindowTitle(QString("About %1").arg(QApplication::applicationName()));
  // Customize the dialog for modelbuilder
  // 1. Provide different clickable URLs.
  aboutDialog.findChild<QLabel*>("ParaViewLinkLabel")
    ->setText("<html><head/><body><p>"
              "<a href=\"https://computationalmodelbuilder.org\">"
              "<span style=\"text-decoration: none; color:palette(link);\">"
              "https://computationalmodelbuilder.org</a>"
              "</span>"
              "</p></body></html>");
  QString version = QString::number(CMB_VERSION_MAJOR) + '.' + QString::number(CMB_VERSION_MINOR) +
    '.' + QString::number(CMB_VERSION_PATCH);
  aboutDialog.findChild<QLabel*>("VersionLabel")->setText(QString("Version: ") + version);
  aboutDialog.findChild<QLabel*>("KitwareLinkLabel")
    ->setText("<html><head/><body><p>"
              "<span style=\"text-decoration: none; color:palette(link);\">"
              "<a href=\"https://kitware.com/\">https://kitware.com/</a>"
              "</span>"
              "</p></body></html>");

  // 2. Add a "licenses" tab for FMA license (and perhaps others)
  auto* tabs = aboutDialog.findChild<QTabWidget*>("tabWidget");
  auto* acknowledgments = new QTextBrowser;
  acknowledgments->setReadOnly(true);
  acknowledgments->setHtml(
    "<html><head/><body><p>"
    "Some of code development is based upon work from the following government"
    " funded efforts: "
    "</p>"
    "<ul>"
    "<li> The U.S. Department of Energy, Office of Science, Chicago Operations Office, under Award "
    "Numbers DE-SC0018055, DE-SC0019786, DE-SC0007615, and DE-SC0019811 *</li>"
    "<li> The National Science Foundation under NSF Grant No. 1450327</li>"
    "<li> The National Institute Of Biomedical Imaging And Bioengineering of the National "
    "Institutes of Health under Award Number R01EB025212 **</li>"
    "</ul>"
    "<p>"
    "As well as contributions made by various commercial activities involving Kitware Inc."
    "</p><p>"
    "<br>"
    "* This work is sponsored by an agency of the United States Government. Neither the United "
    "States Government nor any agency thereof, nor any of their employees, makes any warranty, "
    "express or implied, or assumes any legal liability or responsibility for the accuracy, "
    "completeness, or usefulness of any information, apparatus, product, or process disclosed, or "
    "represents that its use would not infringe privately owned rights. Reference herein to any "
    "specific commercial product, process, or service by trade name, trademark, manufacturer, or "
    "otherwise does not necessarily constitute or imply its endorsement, recommendation, or "
    "favoring by the United States Government or any agency thereof. The views and opinions of "
    "authors expressed herein do not necessarily state or reflect those of the United States "
    "Government or any agency thereof."
    "<br><br>"
    "** The content is solely the responsibility of the authors and does not necessarily represent "
    "the official views of the National Institutes of Health."
    "</p></body></html>");
  tabs->addTab(acknowledgments, "Acknowledgment");
  auto* licenses = new QTextBrowser;
  licenses->setReadOnly(true);
  licenses->setOpenExternalLinks(true);
  tabs->addTab(licenses, "License");
  licenses->setHtml("<html><head/><body>"
                    "<p>"
                    "This program is distributed under the "
                    "<a href=\"https://gitlab.kitware.com/cmb/cmb/-/blob/master/LICENSE.txt\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "3-clause BSD license"
                    "</span>"
                    "</a> provided with its source code. Exceptions are noted below.</p>"
                    "<hr>"

                    "<p>nlohmann_json is distributed under the "
                    "<a href=\"https://github.com/nlohmann/json/blob/develop/LICENSE.MIT\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "MIT License"
                    "</span>"
                    "</a>.</p>"

                    "<p>Boost is distributed under the "
                    "<a href=\"https://www.boost.org/users/license.html\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "Boost Software License"
                    "</span>"
                    "</a>.</p>"

                    "<p>pugi is distributed under the "
                    "<a href=\"https://pugixml.org/license.html\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "MIT License"
                    "</span>"
                    "</a>.</p>"

                    "<p>PEGTL is distributed under the "
                    "<a href=\"https://github.com/taocpp/PEGTL/blob/master/LICENSE\">"
                    "<span style=\"text-decoration: none; color:palette(link);\">"
                    "MIT License"
                    "</span>"
                    "</a>.</p>"

                    "</body></html>");

  // 3. Add version numbers to the client information.
  auto* clientInfo = aboutDialog.findChild<QTreeWidget*>("ClientInformation");
  auto items = clientInfo->findItems("Version", Qt::MatchExactly, 0);
  if (!items.empty())
  {
    (*items.begin())->setText(0, "ParaView Version");
  }
  auto* mbVersion = new QTreeWidgetItem;
  mbVersion->setText(0, "ModelBuilder Version");
  if (CMB_VERSION_PATCH > 0)
  {
    mbVersion->setText(
      1, QString("%1.%2.%3").arg(CMB_VERSION_MAJOR).arg(CMB_VERSION_MINOR).arg(CMB_VERSION_PATCH));
  }
  else
  {
    mbVersion->setText(1, QString("%1.%2").arg(CMB_VERSION_MAJOR).arg(CMB_VERSION_MINOR));
  }
  clientInfo->insertTopLevelItem(0, mbVersion);

  aboutDialog.exec();
}
