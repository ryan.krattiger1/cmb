CMB 6.0 Release Notes
=====================

# General Notes

CMB 6.0 is a complete rewrite of CMB 5. It currently supports a
subset of the functionality (model viewing and attribution) available
in CMB 5.

Whereas CMB 5 was a set of applications derived from ParaView, CMB 6.0
is effectively the ParaView application itself with augmentations and
functionality changes introduced using ParaView's plugin framework.

# New Features

## Resource Panel

CMB 6.0 presents a Resource Panel to display currently loaded models,
meshes and attribute resources. This resource panel is distinct from
ParaView's pipeline panel, whose focus is on visualization pipelines.

## Attribute Panel

Attribute resources loaded into memory do not have a visual
representation in the 3-D render window; instead, they are displayed
in the Attribute Panel. This panel allows the user to create, delete
and modify attributes within the active attribute resource.

## Operation Panel

Operations that act on resources and their components are presented in
the Operation Panel, a preliminary display that adaptively presents
available operations to the user depending on operation's abilities to
act on currently selected objects. An operation is selected by
double-clicking the operation's name. Selecting an operation results
in the operation's interactive input parameters displaying in the
bottom part of the Operation Panel.

## Post-Processing

Because CMB 6.0 is rooted in ParaView, all of ParaView's
post-processing functionality is natively available to the user. To
present a clean interface, much of ParaView's interface is hidden from
the user by default. ParaView's interface can be made available to the
user by loading the post-processing plugin and toggling the ParaView
button on the toolbar.
