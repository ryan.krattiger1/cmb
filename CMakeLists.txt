cmake_minimum_required(VERSION 3.13)

#If the user/superbuild hasn't explicitly stated what c++ standard to use
#require C++11
if(NOT DEFINED CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 11)
  set(CMAKE_CXX_STANDARD_REQUIRED True)
  set(CMAKE_CXX_EXTENSIONS FALSE)
endif()

option(BUILD_SHARED_LIBS "Build CMB using shared libraries" ON)

################################################################################
# CMB version number
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/CMBVersion.cmake)

project(cmb VERSION ${cmb_version})
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(GNUInstallDirs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")

# CMB does not require SMTK to be built but if SMTK is included it provides
# the locations for ParaView and Qt
find_package(smtk QUIET)
if (NOT smtk_FOUND)
  message(WARNING
    "SMTK was not found; this CMB build will not have SMTK plugins built-in.")
endif ()

find_package(ParaView REQUIRED)
find_package(Qt5 REQUIRED COMPONENTS Core Widgets)

foreach (plugin_dependency IN LISTS CMB_EXTRA_SMTK_PLUGINS)
  find_package("${plugin_dependency}" REQUIRED)
endforeach ()

include(CMBPreamble) # Determine architecture, set up build-output directories.
include(CMBTesting) # Add an option to enable testing and set up pointers to the test data.
include(CMBCoverage) # Add options for performing code coverage tests.
include(CMBSanitize) # Add options for performing code sanitization.

# Do not report some warnings from generated code to the dashboard:
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/CTestCustom.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom.cmake")

set(cmb_plugin_paths
  "${cmb_plugin_paths}"
  "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}"
  CACHE STRING "Semi-colon separated paths for testing plugins."
)
mark_as_advanced(cmb_plugin_paths)

add_subdirectory(thirdparty)
add_subdirectory(plugins) # Plugins must be built before the app so .plugins file will be correct.
add_subdirectory(modelbuilder)

option(cmb_enable_documentation "Include targets Sphinx-generated documentation" OFF)
if (cmb_enable_documentation)
  find_package(Sphinx)
  add_subdirectory(doc)
endif()

# Add rules to build packages
include(CMBPackaging)
