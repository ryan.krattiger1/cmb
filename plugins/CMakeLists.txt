paraview_plugin_scan(
  PLUGIN_FILES postprocessing-mode/paraview.plugin
  PROVIDES_PLUGINS paraview_plugins
  ENABLE_BY_DEFAULT ON
  HIDE_PLUGINS_FROM_CACHE ON)
paraview_plugin_build(
  PLUGINS ${paraview_plugins}
  PLUGINS_FILE_NAME "cmb.xml"
  INSTALL_EXPORT CMBParaViewPlugins
  RUNTIME_DESTINATION ${CMAKE_INSTALL_BINDIR}
  CMAKE_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/CMB
  LIBRARY_SUBDIRECTORY "cmb-${cmb_version}"
  TARGET cmb_paraview_plugins)
